var path = require('path');
var express = require('express');
var app = express();
var client_files = path.join(__dirname, 'public');

app.get('/', function(req, res){
	res.sendFile(path.join(client_files, 'index.html'));
});

app.use('/', express.static(__dirname + '/public'));

app.listen(3000);
